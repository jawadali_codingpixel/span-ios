//
//  TaskTracker.swift
//  SpanTest
//
//  Created by Coding Pixel on 21/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class TaskTracker: UITableViewController {//UIViewController , UITableViewDelegate , UITableViewDataSource

    var list = [TaskListModel]()
    
//    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var stepper: MyStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetTaskData()
//        tableview.delegate = self
//        tableview.dataSource = self
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list.count
    }
    
     override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
     override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }

    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TaskTrackerCell
        cell.LblTask.text = list[indexPath.row].Task
        cell.stepper.value = list[indexPath.row].Percent
        return cell
    }
 

     override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Mezzanines"
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let header = view as? UITableViewHeaderFooterView
        header?.contentView.backgroundColor = UIColor(hex: "F9F9F9")
//        header?.textLabel?.font = UIFont(name: "Futura", size: 12) // change it according to ur requirement
        header?.textLabel?.textColor = UIColor(hex: "6D6D72") // change it according to ur requirement
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func GetTaskData()
    {
        list.append(TaskListModel(task: "Unload / Shake Out Inventory", percent: 40))
        list.append(TaskListModel(task: "Primary Framing", percent: 100))
        list.append(TaskListModel(task: "Secondary Framing", percent: 60))
        list.append(TaskListModel(task: "Pressure Wash Framing", percent: 10))
        list.append(TaskListModel(task: "Paint Primary / Secondary Framing", percent: 0))
        list.append(TaskListModel(task: "Equipment Mezzanine", percent: 0))
        list.append(TaskListModel(task: "Electrical Mezzanine", percent: 0))
        list.append(TaskListModel(task: "Gutter System", percent: 0))
        list.append(TaskListModel(task: "Safety Net", percent: 0))
    }
    
    
    
    
    

}
