//
//  LoginViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 29/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var Lbltitle: UILabel!

    @IBOutlet weak var EtPassword: UITextField!
    @IBOutlet weak var EtEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        EtEmail.delegate = self
        EtPassword.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        EtEmail.resignFirstResponder()
        EtPassword.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
