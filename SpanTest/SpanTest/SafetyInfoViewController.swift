//
//  SafetyInfoViewController.swift
//  SpanTest
//
//  Created by MAC MINI on 10/10/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class SafetyInfoViewController: UIViewController {
    @IBOutlet weak var BtnFirstSegment: GreenSMSegmentview!
    @IBOutlet weak var BtnSecondSegment: RedSMSegmentView!
    @IBOutlet weak var BtnThirdSegment: RedSMSegmentView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetupFirstSegment()
        SetupSecondSegment()
        SetupThirdSegment()
        

        // Do any additional setup after loading the view.
    }
    @IBAction func BtnThirdSegmentChange(_ sender: RedSMSegmentView) {
        if sender.selectedSegmentIndex == 0
        {
            self.BtnThirdSegment.segmentAppearance?.segmentOnSelectionColour = UIColor(hex: "11BA06")//UIColor(hex: "D0031B")
        }
        else
        {
            self.BtnThirdSegment.segmentAppearance?.segmentOnSelectionColour = UIColor(hex: "D0031B")
        }
        print("value changed of Third segment \(sender.selectedSegmentIndex)")
    }
    @IBAction func BtnSecondSegmentChange(_ sender: RedSMSegmentView) {
        if sender.selectedSegmentIndex == 0
        {
            self.BtnSecondSegment.segmentAppearance?.segmentOnSelectionColour = UIColor(hex: "D0031B") //UIColor(hex: "D0031B")
        }
        else
        {
            self.BtnSecondSegment.segmentAppearance?.segmentOnSelectionColour = UIColor(hex: "11BA06")
        }
        print("value changed of Second segment \(sender.selectedSegmentIndex)")
    }
//
    @IBAction func BtnFirstSegmentChange(_ sender: GreenSMSegmentview) {
        if sender.selectedSegmentIndex == 0
        {
             self.BtnFirstSegment.segmentAppearance?.segmentOnSelectionColour = UIColor(hex: "D0031B") //UIColor(hex: "D0031B")
        }
        else
        {
             self.BtnFirstSegment.segmentAppearance?.segmentOnSelectionColour = UIColor(hex: "11BA06")
        }
        print("value changed of first segment \(sender.selectedSegmentIndex)")
    }
    func SetupFirstSegment()
    {
        self.BtnFirstSegment.backgroundColor = UIColor.clear
        self.BtnFirstSegment.layer.cornerRadius = 5.0
        self.BtnFirstSegment.layer.borderColor = UIColor(white: 0.85, alpha: 1.0).cgColor
        self.BtnFirstSegment.layer.borderWidth = 1.0
        self.BtnFirstSegment.addSegmentWithTitle("Yes", onSelectionImage: nil, offSelectionImage: nil)
        self.BtnFirstSegment.addSegmentWithTitle("No", onSelectionImage: nil, offSelectionImage: nil)
        
        self.BtnFirstSegment.selectedSegmentIndex = 1
    }
    func SetupSecondSegment()
    {
        self.BtnSecondSegment.backgroundColor = UIColor.clear

        self.BtnSecondSegment.layer.cornerRadius = 5.0
        self.BtnSecondSegment.layer.borderColor = UIColor(white: 0.85, alpha: 1.0).cgColor
        self.BtnSecondSegment.layer.borderWidth = 1.0
        self.BtnSecondSegment.addSegmentWithTitle("Yes", onSelectionImage: nil, offSelectionImage: nil)
        self.BtnSecondSegment.addSegmentWithTitle("No", onSelectionImage: nil, offSelectionImage: nil)
        self.BtnSecondSegment.selectedSegmentIndex = 0
    }
    func SetupThirdSegment()
    {
        self.BtnThirdSegment.backgroundColor = UIColor.clear
        self.BtnThirdSegment.layer.cornerRadius = 5.0
        self.BtnThirdSegment.layer.borderColor = UIColor(white: 0.85, alpha: 1.0).cgColor
        self.BtnThirdSegment.layer.borderWidth = 1.0
        self.BtnThirdSegment.addSegmentWithTitle("Yes", onSelectionImage: nil, offSelectionImage: nil)
        self.BtnThirdSegment.addSegmentWithTitle("No", onSelectionImage: nil, offSelectionImage: nil)
        self.BtnThirdSegment.selectedSegmentIndex = 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
