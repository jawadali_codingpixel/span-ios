//
//  IssueEditViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 28/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class IssueEditViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate{
    
    var list = [ChatListModel]()
    
    var KeyboardHeight: CGFloat = -1
    
    
    @IBOutlet weak var EtComments: UITextField!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var chatview: UIView!
    
    @IBOutlet weak var bottomconstraints: NSLayoutConstraint!
    @IBOutlet weak var LblNoComments: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //GetData()
        tableview.delegate = self
        tableview.dataSource = self
        self.EtComments.delegate = self
        if list.count > 0
        {
            LblNoComments.isHidden = true
        }
        else
        {
            LblNoComments.isHidden = false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        EtComments.resignFirstResponder()
        return true
    }
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.KeyboardHeight == -1
            {
                self.KeyboardHeight = keyboardSize.height
            }
            self.view.frame.origin.y -= self.KeyboardHeight
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y += self.KeyboardHeight
//        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func ChangeEvent(_ sender: UISegmentedControl) {
        EtComments.resignFirstResponder()
    }
    
    override func awakeFromNib() {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.EtComments.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list.count
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ChatMessage = list[indexPath.row]
        
        if ChatMessage.IsMineMessage
        {
            
            if ChatMessage.MessageType == "TEXT"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TestChatCell
                let messageText = list[indexPath.row].TextMessage
                let MaxWidth = UIScreen.main.bounds.width - 50
                let size = CGSize(width: MaxWidth, height: 1000)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)], context: nil)
                cell.viewheight.constant = estimatedFrame.height + 16
                cell.viewwidth.constant = estimatedFrame.width + 32
                cell.bubbleimg.image = #imageLiteral(resourceName: "bubble_blue").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
                cell.LblMessage.text = messageText
                cell.LblMessage.textColor = UIColor.white
                cell.LblMessage.isHidden = false
                cell.bubbleimg.tintColor = UIColor(hex: "4A4A4A")
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OutgoingImageCell", for: indexPath) as! CommentsImageCell
                let WidthAndHeight = UIScreen.main.bounds.width/4
                cell.viewheight.constant = WidthAndHeight
                cell.viewwidth.constant = WidthAndHeight
                cell.IvPhoto.image = #imageLiteral(resourceName: "detail_icon")
                cell.bubbleimg.image = #imageLiteral(resourceName: "bubble_blue").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
                cell.bubbleimg.tintColor = UIColor(hex: "4A4A4A")
                return cell
                
            }
            
        }
        else
        {
            if ChatMessage.MessageType == "TEXT"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "IncomingCell", for: indexPath) as! TestChatCell
                let messageText = list[indexPath.row].TextMessage
                let MaxWidth = UIScreen.main.bounds.width - 50
                let size = CGSize(width: MaxWidth, height: 1000)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)], context: nil)
                cell.viewheight.constant = estimatedFrame.height + 16
                cell.viewwidth.constant = estimatedFrame.width + 32
                cell.bubbleimg.image = #imageLiteral(resourceName: "bubble_gray").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
                cell.LblMessage.text = messageText
                cell.LblMessage.textColor = UIColor.black
                cell.bubbleimg.tintColor = UIColor(hex: "E5E5EA")
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "IncomingImageCell", for: indexPath) as! CommentsImageCell
                let WidthAndHeight = UIScreen.main.bounds.width/4
                cell.viewheight.constant = WidthAndHeight
                cell.viewwidth.constant = WidthAndHeight
                cell.IvPhoto.image = #imageLiteral(resourceName: "detail_icon")
                cell.bubbleimg.image = #imageLiteral(resourceName: "bubble_gray").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
                cell.bubbleimg.tintColor = UIColor(hex: "E5E5EA")
                return cell
            }
            
        }
        
    }
    
    
    
    func GetData()
    {
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "TEXT"))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "TEXT"))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "IMAGE", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "IMAGE", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "IMAGE"))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd j", messagetype: "IMAGE"))
        
    }
    
    
}
