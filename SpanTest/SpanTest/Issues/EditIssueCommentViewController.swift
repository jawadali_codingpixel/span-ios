//
//  EditIssueCommentViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 22/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class EditIssueCommentViewController: UIViewController {

    @IBOutlet weak var BtnSaveContainerView: UIView!
    @IBOutlet weak var BtnSaveContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var IvDescription: UIImageView!
    @IBOutlet weak var IvEdit: UIImageView!
    @IBOutlet weak var tvdescription: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        HideBottomBar()
        IvEdit.layer.zPosition = 1
        IvDescription.layer.zPosition = 1
        
        
        let DescriptionTap = UITapGestureRecognizer(target: self, action: #selector(EditIssueCommentViewController.DescriptionTapped))
        DescriptionTap.numberOfTapsRequired = 1
        IvDescription.isUserInteractionEnabled = true
        IvDescription.addGestureRecognizer(DescriptionTap)
        
        let EditTap = UITapGestureRecognizer(target: self, action: #selector(EditIssueCommentViewController.EditTapped))
        EditTap.numberOfTapsRequired = 1
        IvEdit.isUserInteractionEnabled = true
        IvEdit.addGestureRecognizer(EditTap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Action
    func EditTapped()
    {
        print("Edit Tap on imageview")
        ShowBottomBar()
        IvDescription.image = #imageLiteral(resourceName: "edit")
        IvEdit.isHidden = true
    }
    //Action
    func DescriptionTapped()
    {
        print("Description Tap on imageview")
    }
    
    func HideBottomBar()
    {
        BtnSaveContainerHeight.constant = 0
        BtnSaveContainerView.isHidden = true
    }
    func ShowBottomBar()
    {
        BtnSaveContainerHeight.constant = 45
        BtnSaveContainerView.isHidden = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
