//
//  IssueViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 22/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class IssueViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    var list = [IssueListModel]()
    
    @IBOutlet weak var IssueCategory: UISegmentedControl!
    
    @IBAction func SegmentClick(_ sender: UISegmentedControl)
    {
        if sender.selectedSegmentIndex == 0 {
            GetDraftIssueData()
            tableview.reloadData()
        }
        else if sender.selectedSegmentIndex == 1{
            GetIssueData()
            tableview.reloadData()
        }
        else if sender.selectedSegmentIndex == 2{
            GetAnsweredIssueData()
            tableview.reloadData()
        }
        else if sender.selectedSegmentIndex == 3{
            GetClosedIssueData()
            tableview.reloadData()
        }
    }
    
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        GetIssueData()
        self.tableview.dataSource = self
        self.tableview.delegate = self
        IssueCategory.selectedSegmentIndex = 1
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! IssueCell
        cell.selectionStyle = .none
        cell.LblTitle.text = list[indexPath.row].Title
        cell.LblDateTime.text = list[indexPath.row].DateTime
        cell.LblDescription.text = list[indexPath.row].Description
        if indexPath.row == 1
        {
            cell.BtnRowImg.isHidden = false
        }
        else
        {
            cell.BtnRowImg.isHidden = true
        }
        
        return cell
    }
    
    func GetIssueData()
    {
        list.removeAll()
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
    }
    
    func GetDraftIssueData()
    {
        list.removeAll()
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
    }
    
    func GetAnsweredIssueData()
    {
        list.removeAll()
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
    }
    
    func GetClosedIssueData()
    {
        list.removeAll()
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
        list.append(IssueListModel(title: "#121: Kingspan", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integer.", datetime: "9:22 AM"))
        list.append(IssueListModel(title: "#118: Anvil", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget.", datetime: "8:45 AM"))
        list.append(IssueListModel(title: "#106: McElroy Metals", description: "In hac habitasse platea dictumst. Vivamus adipiscing fermentum quam volutpat aliquam. Integ.", datetime: "8:14 AM"))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onClick_btn_ReportIssue(_ sender: Any)
    {
        
        let newIssueVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportIssueVC") as! ReportIssueVC
        
        self.navigationController?.pushViewController(newIssueVC, animated: true)
    }

}
