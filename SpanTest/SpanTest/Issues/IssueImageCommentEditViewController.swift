//
//  IssueImageCommentEditViewController.swift
//  SpanTest
//
//  Created by Incubasyss on 06/10/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class IssueImageCommentEditViewController: UIViewController {

    @IBOutlet weak var BtnEdit: UIButton!
    @IBOutlet weak var BtnDescription: UIButton!
    @IBOutlet weak var BottomBarTopSpace: NSLayoutConstraint!
    @IBOutlet weak var TopBarBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var TopBarBg: UIImageView!
    @IBOutlet weak var TopBarHeight: NSLayoutConstraint!
    @IBOutlet weak var TvText: UITextView!
    @IBOutlet weak var BottomBarHeight: NSLayoutConstraint!
    @IBOutlet weak var BottomContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var BottomBarView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.OnlyTopBarAppear()
        
    }

    @IBAction func EditDescriptionClick(_ sender: UIButton) {
        self.ShowEditDescription()
    }
    @IBAction func ShowDescriptionClick(_ sender: UIButton) {
        self.ShowDescription()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ShowEditDescription()
    {
        BottomContainerHeight.constant = 200
        BottomBarView.isHidden = false
        BottomBarHeight.constant = 44
        TopBarBottomSpace.constant = 5
        BottomBarTopSpace.constant = 5
        TvText.layer.borderWidth = 1
        TvText.layer.masksToBounds = true
        TvText.isHidden = false
        TvText.isUserInteractionEnabled = true
        self.BtnEdit.isHidden = true
        self.BtnDescription.setImage(#imageLiteral(resourceName: "edit_description"), for: .normal)
    }
    
    func ShowDescription()
    {
        let image = UIImage(named: "descriptionshow") as UIImage?
        self.BtnDescription.setImage(image, for: .normal)
        BottomContainerHeight.constant = 200
        BottomBarView.isHidden = true
        BottomBarHeight.constant = 2
        TopBarBottomSpace.constant = 5
        BottomBarTopSpace.constant = 5
        TvText.layer.borderWidth = 0
        TvText.layer.masksToBounds = true
        TvText.isHidden = false
        TvText.isUserInteractionEnabled = false
    }
    
    func OnlyTopBarAppear()
    {
        BottomContainerHeight.constant = 44
        BottomBarView.isHidden = true
        BottomBarHeight.constant = 0
        TvText.isHidden = true
        TopBarBottomSpace.constant = 0
        BottomBarTopSpace.constant = 0
    }

}
