//
//  IssueCommentViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 22/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class IssueCommentViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate{

    var list = [ChatListModel]()
    
    @IBOutlet weak var CategoryOfIssue: UISegmentedControl!
    @IBOutlet weak var topviewtopconstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var EtComments: UITextField!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var chatview: UIView!
    
    var KeyboardHeigt: CGFloat = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetData()
        tableview.delegate = self
        tableview.dataSource = self
        self.EtComments.delegate = self
        self.CategoryOfIssue.selectedSegmentIndex = 1
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.EtComments.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.KeyboardHeigt == -1
            {
                self.KeyboardHeigt = keyboardSize.height
            }
            self.view.frame.origin.y -= KeyboardHeigt
            
            let indexpath = IndexPath(row: list.count - 1, section: 0)
            self.tableview.scrollToRow(at: indexpath, at: .bottom, animated: true)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y += self.KeyboardHeigt
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func ChangeEvent(_ sender: UISegmentedControl) {
    }
    
    override func awakeFromNib() {
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list.count
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        HighlightCell(indexPath: indexPath, color: UIColor.green)
    }
    
    func HighlightCell(indexPath: IndexPath , color: UIColor)
    {
        if list[indexPath.row].MessageType == "TEXT"
        {
            // Text Message
            let cell = tableview.cellForRow(at: indexPath) as! TestChatCell
            cell.bubbleimg.tintColor = color
        }
        else
        {
            let cell = tableview.cellForRow(at: indexPath) as! CommentsImageCell
            cell.bubbleimg.tintColor = color
            // Image Message
        }

    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if list[indexPath.row].IsMineMessage
        {
            HighlightCell(indexPath: indexPath, color: UIColor(hex: "4A4A4A"))
        }
        else
        {
            HighlightCell(indexPath: indexPath, color: UIColor(hex: "E5E5EA"))
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ChatMessage = list[indexPath.row]
        
        if ChatMessage.IsMineMessage
        {
            
            if ChatMessage.MessageType == "TEXT"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TestChatCell
                let messageText = list[indexPath.row].TextMessage
                let MaxWidth = UIScreen.main.bounds.width - 50
                let size = CGSize(width: MaxWidth, height: 1000)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)], context: nil)
                cell.viewheight.constant = estimatedFrame.height + 16
                cell.viewwidth.constant = estimatedFrame.width + 32
                cell.bubbleimg.image = #imageLiteral(resourceName: "bubble_blue").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
                cell.LblMessage.text = messageText
                cell.LblMessage.textColor = UIColor.white
                cell.LblMessage.isHidden = false
                cell.bubbleimg.tintColor = UIColor(hex: "4A4A4A")
                cell.selectionStyle = .none
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OutgoingImageCell", for: indexPath) as! CommentsImageCell
                let WidthAndHeight = UIScreen.main.bounds.width/4
                cell.viewheight.constant = WidthAndHeight
                cell.viewwidth.constant = WidthAndHeight
                cell.IvPhoto.image = #imageLiteral(resourceName: "detail_icon")
                cell.bubbleimg.image = #imageLiteral(resourceName: "bubble_blue").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
                cell.bubbleimg.tintColor = UIColor(hex: "4A4A4A")
                cell.selectionStyle = .none
                return cell
                
            }
            
        }
        else
        {
            if ChatMessage.MessageType == "TEXT"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "IncomingCell", for: indexPath) as! TestChatCell
                let messageText = list[indexPath.row].TextMessage
                let MaxWidth = UIScreen.main.bounds.width - 50
                let size = CGSize(width: MaxWidth, height: 1000)
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)], context: nil)
                cell.viewheight.constant = estimatedFrame.height + 16
                cell.viewwidth.constant = estimatedFrame.width + 32
                cell.bubbleimg.image = #imageLiteral(resourceName: "bubble_gray").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
                cell.LblMessage.text = messageText
                cell.LblMessage.textColor = UIColor.black
                cell.bubbleimg.tintColor = UIColor(hex: "E5E5EA")
                cell.selectionStyle = .none
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "IncomingImageCell", for: indexPath) as! CommentsImageCell
                let WidthAndHeight = UIScreen.main.bounds.width/4
                cell.viewheight.constant = WidthAndHeight
                cell.viewwidth.constant = WidthAndHeight
                cell.IvPhoto.image = #imageLiteral(resourceName: "detail_icon")
                cell.bubbleimg.image = #imageLiteral(resourceName: "bubble_gray").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
                cell.bubbleimg.tintColor = UIColor(hex: "E5E5EA")
                cell.selectionStyle = .none
                return cell
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.EtComments.endEditing(true)
    }
    
    func handleKeyboardNotification(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            print(keyboardFrame)
            
            let isKeyboardShowing = notification.name == NSNotification.Name.UIKeyboardWillShow
            
            bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in
                
                if isKeyboardShowing {
                    let lastItem = self.list.count - 1
                    let indexPath = IndexPath(item: lastItem, section: 0)
                    self.tableview.scrollToRow(at: indexPath, at: .bottom, animated: true)
//                    self.tableview.scrollToItem(at: indexPath, at: .bottom, animated: true)
                }
                
            })
        }
    }
    
    func GetData()
    {
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "TEXT"))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "TEXT"))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "IMAGE", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "IMAGE", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "TEXT", isminemessage: true))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "IMAGE"))
        list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu", messagetype: "IMAGE"))
        
    }


}
