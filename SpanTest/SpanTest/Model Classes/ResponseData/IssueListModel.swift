//
//  IssueListModel.swift
//  SpanTest
//
//  Created by Coding Pixel on 22/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import Foundation

class IssueListModel
{
    var Title: String
    var Description: String
    var DateTime: String
    
    init(title: String , description: String , datetime: String)
    {
        self.Title = title
        self.Description = description
        self.DateTime = datetime
    }
    
    
    
}
