//
//  DailyOverviewListModel.swift
//  SpanTest
//
//  Created by Coding Pixel on 05/10/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import Foundation
class DailyOverviewListModel
{
    var IsFavorite: Bool
    
    init(isfavorite: Bool)
    {
        self.IsFavorite = isfavorite
    }
    
}
