//
//  ChatListModel.swift
//  SpanTest
//
//  Created by Coding Pixel on 28/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import Foundation
class ChatListModel
{
    var DateTime: String
    var TextMessage: String
    var MessageType: String
    var IsMineMessage: Bool
    
    init(datetime: String , textmessage: String , messagetype: String , isminemessage: Bool = false)
    {
        DateTime = datetime
        TextMessage = textmessage
        MessageType = messagetype
        IsMineMessage = isminemessage
    }
    
    
    
    
}
