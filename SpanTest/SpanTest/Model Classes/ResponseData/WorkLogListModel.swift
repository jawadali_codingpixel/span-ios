//
//  WorkLogListModel.swift
//  SpanTest
//
//  Created by Coding Pixel on 05/10/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import Foundation
class WorkLogListModel{
    
    var Project: String
    var WorkingHours: Int
    var TodayWorkingHours: Int
    
    
    init(project: String , workinghours: Int , todayworkinghours: Int)
    {
        self.Project = project
        self.WorkingHours = workinghours
        self.TodayWorkingHours = todayworkinghours
    }
    
    
}
