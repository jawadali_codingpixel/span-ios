//
//  ProjectModel.swift
//  SpanTest
//
//  Created by Coding Pixel on 21/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import Foundation
class ProjectModel
{
    var Project: String
    var Data: String
    
    init(project: String , data: String)
    {
        self.Project = project
        self.Data = data
    }
}
