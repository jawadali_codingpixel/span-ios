//
//  MaterialListModel.swift
//  SpanTest
//
//  Created by Coding Pixel on 22/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import Foundation
class MaterialListModel
{
    var Material: String
    var DateTime: String
    init(material: String , datetime: String)
    {
        self.Material = material
        self.DateTime = datetime
    }
}
