//
//  TaskListModel.swift
//  SpanTest
//
//  Created by Coding Pixel on 21/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import Foundation
class TaskListModel
{
    var Task: String
    var Percent: Double
    
    init(task: String , percent: Double)
    {
        self.Task = task
        self.Percent = percent
    }
    
}
