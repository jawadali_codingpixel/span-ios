//
//  DashboardListModel.swift
//  SpanTest
//
//  Created by Coding Pixel on 25/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import Foundation
class DashboardListModel
{
    var Title: String
    var Date: String
    
    init(title: String , date: String)
    {
        self.Title = title
        self.Date = date
    }
    
}
