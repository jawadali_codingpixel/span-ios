//
//  DashboardViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 25/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{

    var list = [DashboardListModel]()
    
    @IBOutlet weak var MaterialViewContainer: UIView!
    
    @IBOutlet weak var LblEndTime: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var LblForenHeight: UILabel!
    @IBOutlet weak var LblForenheightSecond: UILabel!
    @IBOutlet weak var LblForenheightThird: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        GetData()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        LblForenHeight.text = "72"+String(UnicodeScalar(UInt8(186)))+" F"
        LblForenheightSecond.text = "71"+String(UnicodeScalar(UInt8(186)))+" F"
        LblForenheightThird.text = "68"+String(UnicodeScalar(UInt8(186)))+" F"
//        LblEndTime.text = String(UnicodeScalar(UInt8(196)))+":"+String(UnicodeScalar(UInt8(196)))
        
        
//        MaterialViewContainer.layer.shadowColor = UIColor.red.cgColor
//        MaterialViewContainer.layer.shadowOpacity = 1
//        MaterialViewContainer.layer.shadowOffset = CGSize.zero
//        MaterialViewContainer.layer.shadowRadius = 10
//        MaterialViewContainer.layer.shadowPath = UIBezierPath(rect: MaterialViewContainer.bounds).cgPath
//
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DashboardCell
        cell.LblTitle.text = list[indexPath.row].Title
        cell.LblDate.text = list[indexPath.row].Date
        
        return cell
        
    }
    
    
    func GetData()
    {
        list.append(DashboardListModel(title: "#178", date: "06/20/17"))
        list.append(DashboardListModel(title: "#177", date: "06/19/17"))
        list.append(DashboardListModel(title: "#176", date: "06/18/17"))
        list.append(DashboardListModel(title: "#175", date: "06/17/17"))
        list.append(DashboardListModel(title: "#174", date: "06/16/17"))
        list.append(DashboardListModel(title: "#173", date: "06/15/17"))
        list.append(DashboardListModel(title: "#172", date: "06/14/17"))
        list.append(DashboardListModel(title: "#171", date: "06/13/17"))
        list.append(DashboardListModel(title: "#170", date: "06/12/17"))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
