//
//  ChatsViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 27/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class ChatsViewController: UITableViewController {

    
    var list = [ChatListModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GetData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if list[indexPath.row].IsMineMessage {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ChatCell
            cell.LblMessage.layer.zPosition = 1
            
            cell.MessageBubbleImg.image = #imageLiteral(resourceName: "bubble_blue").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
            cell.MessageBubbleImg.tintColor = UIColor(hex: "4A4A4A")
            let text = list[indexPath.row].TextMessage
            cell.LblMessage.text = text
            
            let width = UIScreen.main.bounds.width - 100
            
            let size = NSString(string: text).boundingRect(with: CGSize(width: width , height: 1000),
                                                           options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                           attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)],
                                                           context: nil).size
            
            cell.ChatBubbleWidth.constant = size.width + 40
            cell.ChatBubbleHeight.constant = size.height + 16
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "IncomingCell", for: indexPath) as! ChatCell
            cell.LblMessage.layer.zPosition = 1
            
            cell.MessageBubbleImg.image  = #imageLiteral(resourceName: "bubble_gray").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
            cell.MessageBubbleImg.tintColor = UIColor(hex: "E5E5EA")
            let text = list[indexPath.row].TextMessage
            cell.LblMessage.text = text
            cell.LblMessage.textColor = UIColor.black
            
            let width = UIScreen.main.bounds.width - 100
            
            let size = NSString(string: text).boundingRect(with: CGSize(width: width , height: 1000),
                                                           options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                           attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)],
                                                           context: nil).size
            
            cell.ChatBubbleWidth.constant = size.width + 40
            cell.ChatBubbleHeight.constant = size.height + 16
            return cell
        }
//        IncomingCell
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ChatCell
////        self.view.bringSubview(toFront: cell.LblMessage)
//        cell.LblMessage.layer.zPosition = 1
//        cell.MessageBubbleImg.image = #imageLiteral(resourceName: "bubble_blue").resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
//        cell.MessageBubbleImg.tintColor = UIColor(hex: "4A4A4A")
//        let text = "dsjkdhsjdhs sdjsdhjshdjs dsdsjdhjsdh dhsjdhsjkdhjsd jsdhjsdhjkshd hsjdhjkshds hjshdjksdhs hkjhjkshd jkhdjshdjsa kjdksjds kdjksldjksd skdjksldjs kjsakdjaklsd jdkasjdkasdj jkjdas daks jksdjksadjs jdskldjaklsdj jskldjs dskj  jksdjklsajdsakl jsdjaklsjdklsdj jsldjsakldjskdj jksdjskldj "
//        cell.LblMessage.text = text
//        
//        let width = UIScreen.main.bounds.width - 50
//        
//        let size = NSString(string: text).boundingRect(with: CGSize(width: width , height: 1000),
//                                                         options: NSStringDrawingOptions.usesLineFragmentOrigin,
//                                                         attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)],
//                                                         context: nil).size
//        
//        cell.ChatBubbleWidth.constant = size.width
//        cell.ChatBubbleHeight.constant = size.height + 8

    }


 
    func GetData()
    {
        for _ in 1...20
        {
            list.append(ChatListModel(datetime: "Michael Scott - Today 9:51 AM", textmessage: "hi this is issues comments for feedback.jkjkasdjksd jsdksjd ", messagetype: "TEXT", isminemessage: false))
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
