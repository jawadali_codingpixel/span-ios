//
//  OutgoingImageCell.swift
//  SpanTest
//
//  Created by Coding Pixel on 28/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class CommentsImageCell: UITableViewCell {

    @IBOutlet weak var ImageContainerView: UIView!
    @IBOutlet weak var IvPhoto: UIImageView!
    @IBOutlet weak var bubbleimg: UIImageView!
    @IBOutlet weak var viewheight: NSLayoutConstraint!
    @IBOutlet weak var viewwidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
