//
//  TestChatCell.swift
//  SpanTest
//
//  Created by Coding Pixel on 28/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class TestChatCell: UITableViewCell {
    @IBOutlet weak var LblMessage: UILabel!
    @IBOutlet weak var viewwidth: NSLayoutConstraint!
    @IBOutlet weak var viewheight: NSLayoutConstraint!
    @IBOutlet weak var bubbleimg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    override var isHighlighted: Bool{
//        didSet{
//            bubbleimg.tintColor = isHighlighted ? UIColor.green : UIColor(hex: "4A4A4A")
//            print("Highlighted ")
//        }
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
