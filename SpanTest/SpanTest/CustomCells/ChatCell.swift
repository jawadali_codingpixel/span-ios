//
//  ChatCell.swift
//  SpanTest
//
//  Created by Coding Pixel on 27/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet weak var ChatBubbleWidth: NSLayoutConstraint!
    @IBOutlet weak var ChatBubbleHeight: NSLayoutConstraint!
    @IBOutlet weak var MessageBubbleImg: UIImageView!
    @IBOutlet weak var LblMessage: UILabel!
    @IBOutlet weak var MessageWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
