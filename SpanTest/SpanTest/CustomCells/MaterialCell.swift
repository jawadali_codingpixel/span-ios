//
//  MaterialCell.swift
//  SpanTest
//
//  Created by Coding Pixel on 22/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class MaterialCell: UITableViewCell {

    @IBOutlet weak var LblMaterial: UILabel!
    @IBOutlet weak var LblDateTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
