//
//  WorkLogCell.swift
//  SpanTest
//
//  Created by Coding Pixel on 25/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class WorkLogCell: UITableViewCell {

    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var SvLabels: UIStackView!
    @IBAction func BtnShowClick(_ sender: UIButton) {
    }
    
    var isExpanded:Bool = false
        {
        didSet
        {
            if !isExpanded {
                self.height.constant = 0.0
                
            } else {
                self.height.constant = 100.0
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
