//
//  ExpandableListCell.swift
//  SpanTest
//
//  Created by Coding Pixel on 26/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class ExpandableListCell: UITableViewCell {
    @IBOutlet weak var TodayWorkingHours: UILabel!
    @IBOutlet weak var ProjectWorkingHours: UILabel!
    @IBOutlet weak var IvClock: UIImageView!
    @IBOutlet weak var LblProject: UILabel!
    @IBOutlet weak var expandcollapseicon: UIImageView!
    @IBOutlet weak var ContainerView: UIView!
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var expandableview: UIView!
    @IBOutlet weak var separator: UILabel!
    @IBOutlet weak var height: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var isExpanded:Bool = false
        {
        didSet
        {
            if !isExpanded {
                self.TopView.layer.borderWidth = 1
                self.TopView.layer.borderColor = UIColor(hex: "D9D9DB").cgColor
                
                self.ContainerView.layer.borderWidth = 0
                self.ContainerView.layer.borderColor = UIColor(hex: "D9D9DB").cgColor
                
                self.height.constant = 0.0
                self.expandableview.isHidden = true
                self.expandcollapseicon.image = #imageLiteral(resourceName: "arrow_down")
//                                self.ExpandableViewHeightConstantraint.constant = 50.0
//                                self.ExpandableView.isHidden = true
                
            } else {
//                                self.ExpandableView.isHidden = false
                self.ContainerView.layer.borderWidth = 1
                self.ContainerView.layer.borderColor = UIColor(hex: "D9D9DB").cgColor
                
                self.TopView.layer.borderWidth = 0
                self.TopView.layer.borderColor = UIColor(hex: "D9D9DB").cgColor
                self.height.constant = 260
                self.expandableview.isHidden = false
                self.expandcollapseicon.image = #imageLiteral(resourceName: "arrow_up")
//                                self.ExpandableViewHeightConstantraint.constant = 50.0
//                self.MainViewHeightConstraints.constant = 150
            }
            
        }
    }

}
