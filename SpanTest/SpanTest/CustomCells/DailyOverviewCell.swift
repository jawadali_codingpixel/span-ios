//
//  DailyOverviewCell.swift
//  SpanTest
//
//  Created by Coding Pixel on 25/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class DailyOverviewCell: UICollectionViewCell {
    @IBOutlet weak var IvFavorite: UIImageView!
    @IBOutlet weak var IvPicThumbnail: UIImageView!
    @IBOutlet weak var IvUpload: UIImageView!
}
