//
//  DailyOverviewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 25/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class DailyOverviewController: UIViewController , UICollectionViewDataSource{
    @IBOutlet weak var collectionview: UICollectionView!
    
    var list = [DailyOverviewListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetData()
        collectionview.dataSource = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! DailyOverviewCell
        if indexPath.row == 0
        {
            cell.IvFavorite.isHidden = true
            cell.IvPicThumbnail.image = #imageLiteral(resourceName: "upload")
            cell.IvUpload.isHidden = false
            cell.backgroundColor = UIColor.white
        }
        else
        {
            cell.IvUpload.isHidden = true
            cell.IvPicThumbnail.image = #imageLiteral(resourceName: "cloudyy")
            cell.backgroundColor = UIColor(hex: "F1F1F1")
            cell.IvFavorite.isHidden = false
            if list[indexPath.row].IsFavorite
            {
                cell.IvFavorite.image = #imageLiteral(resourceName: "favorite")
            }
            else
            {
                cell.IvFavorite.image = #imageLiteral(resourceName: "unfavorite")
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10); //UIEdgeInsetsMake(topMargin, left, bottom, right);
    }
    

    func GetData()
    {
        list.append(DailyOverviewListModel(isfavorite: true))
        list.append(DailyOverviewListModel(isfavorite: false))
        list.append(DailyOverviewListModel(isfavorite: true))
        list.append(DailyOverviewListModel(isfavorite: false))
        list.append(DailyOverviewListModel(isfavorite: true))
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
