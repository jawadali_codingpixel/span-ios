//
//  UITextField+Utils.swift
//  MyTutorLabSwift
//
//  Created by Saad Chaudhry on 10/07/2017.
//  Copyright © 2017 sdsol. All rights reserved.
//

import Foundation
extension UITextField
{
    func setLeftPadding()
    {
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        self.leftView = leftView
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    func highlight()
    {
        let aView = self
        aView.layer.masksToBounds = true
        aView.layer.borderColor = UIColor.red.cgColor
        aView.layer.borderWidth = 1
    }
    

    func unhighlight()
    {
        let aView = self
        aView.layer.borderColor = UIColor.clear.cgColor
    }
    
    
    func isValidEmail() -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text)
    }
    
    
    func setRightImageWithPadding(_ imageName: String)
    {
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        self.leftView = leftView
        self.leftViewMode = UITextFieldViewMode.always
        
        
        let rightImage = UIImageView()
        rightImage.frame = CGRect(x: 0, y: 0, width: 20, height: 10)
        rightImage.image = UIImage.init(named: imageName)
        
        
        let rightView = UIView()
        rightView.frame = CGRect(x: 0, y: 0, width: 30, height: 10)
        
        rightView.addSubview(rightImage)
        
        self.rightView = rightView
        self.rightViewMode = UITextFieldViewMode.always

        
    }
    
    
}



/*
 
 + (void) setRightImageWithPadding:(UIView*)view withImage:(NSString *)imageName
 {
 if ([view isKindOfClass:[UITextField class]] || [view isKindOfClass:[UITextView class]])
 {
 UIImageView *leftPadding           = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,10,20)];
 
 leftPadding.backgroundColor        = [UIColor clearColor];
 
 UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 22, 15)];
 
 UIImageView *rightPadding          = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,15,15)];
 rightPadding.contentMode           = UIViewContentModeScaleAspectFit;
 
 rightPadding.image                 = [UIImage imageNamed:imageName];
 
 [rightView addSubview:rightPadding];
 
 rightView.userInteractionEnabled = false;
 
 leftPadding.contentMode            = UIViewContentModeScaleAspectFit;
 ((UITextField*)view).leftView      = leftPadding;
 ((UITextField*)view).rightView     = rightView;
 ((UITextField*)view).leftViewMode  = UITextFieldViewModeAlways;
 ((UITextField*)view).rightViewMode = UITextFieldViewModeUnlessEditing;
 }
 }

 
 */
