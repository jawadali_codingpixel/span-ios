    //
//  Utilities.swift
//  MyTutorLabSwift
//
//  Created by Saad Chaudhry on 10/07/2017.
//  Copyright © 2017 sdsol. All rights reserved.
//

import UIKit

class Utilities: NSObject
{

   static func saveUserDefaults(_ value: Any,forKey key: String)
    {
        UserDefaults.standard.set(value, forKey: key)
    }
    
   static func getUserDefaults(_ key: String) -> Any
    {
        return UserDefaults.standard.value(forKey: key) ?? ""
    }
    
    static func archiveObject (_ value : Any) -> Any
    {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: value)
        
        return encodedData
    }
    
    static func unArchiveObject (_ value : Any) -> Data
    {
      return  NSKeyedUnarchiver.unarchiveObject(with: value as! Data) as! Data
    }
    
    
    static func showHUD()
    {
        self.removeHUD()
        
        let app = UIApplication.shared.delegate as? AppDelegate
        
        let hud = MBProgressHUD.showAdded(to: app?.window, animated: true)
        hud?.removeFromSuperViewOnHide = true
        app?.window?.addSubview(hud!)
       // hud?.labelText = text
        hud?.show(true)
    }

    
    
    static func showHUDWithText(_ text: String)
    {
        self.removeHUD()
        
        let app = UIApplication.shared.delegate as? AppDelegate
        
        let hud = MBProgressHUD.showAdded(to: app?.window, animated: true)
        hud?.removeFromSuperViewOnHide = true
        app?.window?.addSubview(hud!)
        hud?.labelText = text
        hud?.show(true)
    }
    
    static func removeHUD()
    {
        let app = UIApplication.shared.delegate as? AppDelegate

        MBProgressHUD.hide(for: app?.window, animated: true)
    }
    
    static func showProgressHUD(_ view: UIView)
    {
        self.removeHUD()
        
        var mbroundProgress = MBRoundProgressView()
        
        DispatchQueue.main.async {
            let hud = MBProgressHUD.showAdded(to: view, animated: true)
            hud?.mode = MBProgressHUDModeCustomView
            mbroundProgress = MBRoundProgressView.init(frame: CGRect(x: 0, y: 0, width: 37, height: 37))
            hud?.customView = mbroundProgress
            
            mbroundProgress.progress = 0.0
            mbroundProgress.isAnnular = true
            mbroundProgress.progressTintColor = UIColor.purple
            mbroundProgress.backgroundTintColor = UIColor.red
            
            
            hud?.labelText = "Uploading..."
            
            view.addSubview(hud!)
            
            hud?.show(true)
        }
        
    }
    
    
    static func localToUTC(date:String) -> String
    {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "MM/dd/yyyy, h:mm a"
        dateFormator.calendar = NSCalendar.current
        dateFormator.timeZone = TimeZone.current
        
        let dt = dateFormator.date(from: date)
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        dateFormator.dateFormat = "MM/dd/yyyy h:mm a"
        
        return dateFormator.string(from: dt!)
    }
    
    static func LocalUTCToLocal(date:String) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "MM/dd/yyyy h:mm a"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        
        
        let adate = dateFormator.date(from: date)!
        
        
        
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat = "MM/dd/yyyy, h:mm a"
        
        return dateFormator.string(from: adate)
    }
    
    // 08/21/2017 9:00 PM
    static func UTCToLocal(date:String) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "MM/dd/yyyy h:mm a"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        
       
        let adate = dateFormator.date(from: date)!
        
        
       
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat = "MM/dd/yyyy h:mm a"
        
        return dateFormator.string(from: adate)
    }

    static func UTCToLocalTime(date: String) -> String
    {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "MM/dd/yyyy h:mm a"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        let adate = dateFormator.date(from: date)!
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "hh:mm a"
        
        return dateFormatter1.string(from: adate)
        
    }
    
    static func UTCTimeToLocalTime(date: String) -> String
    {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "h:mm a"
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        
        let adate = dateFormator.date(from: date)!
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "hh:mm a"
        
        return dateFormatter1.string(from: adate)
        
    }
    
    static func localTimeToUTCTime(date:String) -> String
    {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "h:mm a"
        dateFormator.calendar = NSCalendar.current
        dateFormator.timeZone = TimeZone.current
        
        let dt = dateFormator.date(from: date)
        dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        dateFormator.dateFormat = "h:mm a"
        
        return dateFormator.string(from: dt!)
    }
    
   
    
}
