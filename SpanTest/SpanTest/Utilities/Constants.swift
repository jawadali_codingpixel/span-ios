//
//  Constants.swift
//  MyTutorLabSwift
//
//  Created by Saad Chaudhry on 06/07/2017.
//  Copyright © 2017 sdsol. All rights reserved.
//

import Foundation

struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}


struct Constants
{

    
    static let IS_IPHONE_5  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    
    static let appdelegate = UIApplication.shared.delegate as! AppDelegate
    

    static let green_Color            = "357983"
    static let pink_Color             = "E74B64"
    static let grey_Color             = "898989"
    static let position               = "center"
    static let defaultDuration : Int  = 2

    static let userEmail              = "email"
    static let userPassword           = "password"
    static let fbID                   = "fbID"

    static let Subject                = "Subject"
    static let Course                 = "Course"
    static let Tier                   = "Tier"
    static let Rating                 = "Rating"
    
    static let ConfirmBooking                 = "ConfirmBooking"

    static let xmppBaseURL                = "http://staging3.sdsol.com/MyTutorLab/api/v1/"
    
    static let baseURL                = "http://staging3.sdsol.com/MyTutorLab/api/v1/Users/"

    static let baseTutorURL           = "http://staging3.sdsol.com/MyTutorLab/api/v1/Tutor/"

    static let baseSessionURL         = "http://staging3.sdsol.com/MyTutorLab/api/v1/Sessions/"

    static let baseParentURL          = "http://staging3.sdsol.com/MyTutorLab/api/v1/Parent/"
    
    static let baseStudentURL         = "http://staging3.sdsol.com/MyTutorLab/api/v1/Student/"

    static let METERS_PER_MILE: Float = 1609.344
    
   
}

