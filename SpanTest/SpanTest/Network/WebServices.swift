//
//  ReportIssueVC.swift
//  SpanTest
//
//  Created by Saad Chaudhry Macbook on 24/10/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum ApiName
{
    case eApiLogin
    case eApiSigninThirdParty
    case eApiStudentSignup
    case eApiParentSignup
    case eApiGetForgetPassword
    case eApiSaveTutorCurriculum
    case eApiGetTutorCurriculums
    case eApiGetUserProfile
    case eApiContactUs
    case eApiGetTutorAvailability
    case eApiSaveTutorAvailability
    case eApiGetAllCurriculums
    case eApiSaveAvailableNow
    case eApiUpdatePassword
    case eApiUpdateUserPicture
    case eApiUpdateUserSetting
    case eApiUpdateTutor
    case eApiGetTutorSessions
    case eApiGetSubjectsList
    case eApiGetCoursesList
    case eApiRequestCurriculum
    case eApiUpdateSessionRequest
    case eApiTutorSessionViewed
    case eApiGetTutorSessionsCalendar
    case eApiGetTutorSessionsCalendarDetails
    case eApiBrowseCurriculumORTutors
    case eApiGetChildsList
    case eAPiSearchORFindTutors
    case eApiBookTutor
    case eApiGetTiersList
    case eAPiGetParentSessions
    case eApiGetBraintreeAccessToken
    case eApiAuthorizePayment
    case eApiSaveChildInfo
    case eApiUpdateParent
    case eApiCancelSession
    case eApiUpdatePaypalEmail
    case eApiCheckforPendingSessionRating
    case eApiAddTutorRating
    case eApiCreateGroup
    case eApiResetNotification
    case eApiUpdateSession
}

protocol WebServicesDelegate: class
{
    
    func didReceiveResponseOfApiOnSuccess(apiName: ApiName, response:Any)
    func didReceiveResponseOfApiOnFailure(apiName: ApiName, error : Error?)
    func didReceiveResponseOfApiOnFailure(apiName: ApiName, error : Error?, message:String!)
}

extension WebServicesDelegate {
    
    func didReceiveResponseOfApiOnSuccess(apiName: ApiName, response:Any){}
    func didReceiveResponseOfApiOnFailure(apiName: ApiName, error : Error?){}
    func didReceiveResponseOfApiOnFailure(apiName: ApiName, error : Error?, message:String!){}
}

class WebServices: NSObject
{
    
    static let sharedInstance = WebServices()
    //weak var delegate:WebServicesDelegate?
    
    //This prevents others from using the default '()' initializer for this class.
    private  override init()
    {
        super.init()
    }
    
    private func clearPreviosCookies(_ urlStr:String)
    {
        let url = URL(string: urlStr)
        
        let cstorage = HTTPCookieStorage.shared
        if let cookies = cstorage.cookies(for: url!) {
            for cookie in cookies {
                cstorage.deleteCookie(cookie)
            }
        }
    }
    
    func authorizedRequest(
        _ url: String!,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default
        )
        -> DataRequest
    {
        
    //    let user = "sdsol"
   //     let password = "sdsol99"
        
   //     let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
    //    let base64Credentials = credentialData.base64EncodedString(options: [])
        
      //  let headers = []
        
      //  let headers: HTTPHeaders = ["Authorization": "Basic \(base64Credentials)" , "Accept": "application/json"]
        
     //   let encodedURL = url.fastestEncoding
     //   print("Request Url : \(encodedURL)" )
        
        let request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: nil).validate()
        


        return request
    }
    
    func authorizedURLRequest(
        _ url: String!,
        method: HTTPMethod = .get,
        parameters: Any? = nil        )
        -> DataRequest
    {
        
        let user = "sdsol"
        let password = "sdsol99"
        
        //        let userName = "20170000"
        //        let password = "3579510"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        
        
        var urlRequest = URLRequest(url: NSURL(string: url)! as URL)
        if method == .get
        {
             urlRequest.httpMethod = "GET"
        }
        else if method == .put
        {
            urlRequest.httpMethod = "PUT"
        }
        else
        {
        urlRequest.httpMethod = "POST"
        }
        
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters ?? "" , options: [])
        } catch {
            // No-op
        }
        
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("Basic \(base64Credentials)" , forHTTPHeaderField: "Authorization")
        
        
        return Alamofire.request(urlRequest)
    }
    
    func authorizedMultipartURLRequest(
        _ url: String!,
        method: HTTPMethod = .get,
        parameters: Any? = nil        )
        -> DataRequest
    {
        
        let user = "sdsol"
        let password = "sdsol99"
        
        //        let userName = "20170000"
        //        let password = "3579510"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        
        
        var urlRequest = URLRequest(url: NSURL(string: url)! as URL)
        if method == .get
        {
            urlRequest.httpMethod = "GET"
        }
        else if method == .put
        {
            urlRequest.httpMethod = "PUT"
        }
        else
        {
            urlRequest.httpMethod = "POST"
        }
        
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters ?? "" , options: [])
        } catch {
            // No-op
        }
        
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("Basic \(base64Credentials)" , forHTTPHeaderField: "Authorization")
        
        
                 
        return Alamofire.request(urlRequest)
    }


    
    
    // MARK: ParentSignUp
    func parentSignupWithParams(_ params: SignInParams, delegate:WebServicesDelegate, showHud:Bool? = true)
    {
        
        if(showHud)!{   Utilities.showHUD()}
        
        
        let URL = Constants.baseURL + "ParentSignup"
       
        
        print("Url : \(URL)")
        
        print("Params : \(params)")
        
        
        let json = JSONSerializer.toJson(params)
        
        print(json)
        
        do {
            
            let dict =  try JSONSerializer.toDictionary(json)
            
            print("Login Params are \(dict)")
            
            self.authorizedRequest(URL, method: .post, parameters: dict as? Parameters).responseObject(completionHandler: { (response: DataResponse<SignInParams>) in
                
                print("original URL request : \(String(describing: response.request))")
                print("server data : \(response.description)")
                
                
                let loginArray = response.result.value
                
                print(loginArray ?? "")
                
                
              //  print("Response Code: \(String(describing: loginArray?.Response?.Code))")
                
                if(loginArray != nil)
                {
//                    Utilities.saveUserDefaults(params.Email, forKey: Constants.userEmail)
//                    Utilities.saveUserDefaults(params.Password, forKey: Constants.userPassword)
                    
                    delegate.didReceiveResponseOfApiOnSuccess(apiName: .eApiParentSignup, response: loginArray!)
                }
                else
                {
                    let error = response.error
                    if (error != nil)
                    {
                        delegate.didReceiveResponseOfApiOnFailure(apiName: .eApiGetSubjectsList, error: error, message: "")
                    }
                    else
                    {
                     //   delegate.didReceiveResponseOfApiOnFailure(apiName: .eApiGetSubjectsList, error: error, message: loginArray?.Response?.Status)
                    }
                    
                    
                }
                
                if(showHud)!{ Utilities.removeHUD() }
                
                
            })
            
            
        } catch
        {
            print("error")
        }
        
    }
    
    
    
    
    
    //MARK: Convert To Dictionary
    func convertToDictionary(text: String) -> [String: Any]? {
        var dictonary:NSDictionary?
        if let data = text.data(using: .utf8) {
            do {
                  dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as NSDictionary?
                return dictonary as! [String : Any]?
                
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    
}
