//
//  WorkLogViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 25/09/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class WorkLogViewController: UITableViewController {

    var expandedRows = Set<Int>()
    var list = [WorkLogListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ExpandableListCell

        cell.LblProject.text = list[indexPath.row].Project
        cell.ProjectWorkingHours.text = "\(list[indexPath.row].WorkingHours)h"
        if list[indexPath.row].TodayWorkingHours > 0
        {
            cell.IvClock.image = #imageLiteral(resourceName: "green_clock")
            cell.ProjectWorkingHours.textColor = UIColor(hex: "3BC433")
            cell.TodayWorkingHours.text = "+\(list[indexPath.row].TodayWorkingHours)h"
        }
        else
        {
            cell.IvClock.image = #imageLiteral(resourceName: "clock")
            cell.ProjectWorkingHours.textColor = UIColor.black
            cell.TodayWorkingHours.text = ""
        }
        
        // Configure the cell...
        cell.isExpanded = self.expandedRows.contains(indexPath.row)
        cell.selectionStyle = .none

        return cell
    }
 

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    // TableView Delegate methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(indexPath.row)
        guard let cell = tableView.cellForRow(at: indexPath) as? ExpandableListCell
            else { return }
//        cell.LblProject.text = list[indexPath.row].Project
//         cell.ProjectWorkingHours.text = "\(list[indexPath.row].WorkingHours)h"
//        if list[indexPath.row].TodayWorkingHours > 0
//        {
//            cell.IvClock.image = #imageLiteral(resourceName: "green_clock")
//            cell.ProjectWorkingHours.textColor = UIColor(hex: "3BC433")
//            cell.TodayWorkingHours.text = "+\(list[indexPath.row].TodayWorkingHours)h"
//        }
//        else
//        {
//            cell.IvClock.image = #imageLiteral(resourceName: "clock")
//            cell.ProjectWorkingHours.textColor = UIColor.black
//            cell.TodayWorkingHours.text = ""
//        }
        switch cell.isExpanded
        {
        case true:
            self.expandedRows.remove(indexPath.row)
        case false:
            self.expandedRows.insert(indexPath.row)
        }
        
        
        cell.isExpanded = !cell.isExpanded
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ExpandableListCell
            else { return }
        
        self.expandedRows.remove(indexPath.row)
        
        cell.isExpanded = false
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        
    }
    
    func GetData()
    {
        list.append(WorkLogListModel(project: "Metal Wall Panels", workinghours: 32 , todayworkinghours: 0))
        list.append(WorkLogListModel(project: "Texture Wall Panels", workinghours: 9 , todayworkinghours: 3))
        list.append(WorkLogListModel(project: "Entry Canopy", workinghours: 12 , todayworkinghours: 6))
        list.append(WorkLogListModel(project: "Mezzanines", workinghours: 0 , todayworkinghours: 0))
        list.append(WorkLogListModel(project: "Texture Wall Panels", workinghours: 4 , todayworkinghours: 4))
        list.append(WorkLogListModel(project: "Machine Room", workinghours: 0 , todayworkinghours: 0))
        list.append(WorkLogListModel(project: "Power Washing / Painting", workinghours: 12 , todayworkinghours: 0))
        list.append(WorkLogListModel(project: "Metal Wall Panels", workinghours: 12 , todayworkinghours: 0))
        list.append(WorkLogListModel(project: "Metal Wall Panels", workinghours: 12 , todayworkinghours: 0))
        list.append(WorkLogListModel(project: "Metal Wall Panels", workinghours: 12 , todayworkinghours: 0))
    }

}
