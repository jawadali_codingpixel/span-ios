//
//  SignInParams.swift
//  SpanTest
//
//  Created by Saad Chaudhry on 24/10/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit
import ObjectMapper
class SignInParams: NSObject,Mappable
{
    override init()
    {
        
    }
    
    required init?(map: Map)
    {
        
    }
    
    func mapping(map: Map)
    {
        
    }
    
    var Email        : String? = ""
    var Password     : String? = ""

}
