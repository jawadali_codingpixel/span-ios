//
//  SettingsViewController.swift
//  SpanTest
//
//  Created by Coding Pixel on 02/10/2017.
//  Copyright © 2017 Coding Pixel. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var EtConfirmPassword: UITextField!
    @IBOutlet weak var EtCurrentPassword: UITextField!
    @IBOutlet weak var EtNewPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        EtNewPassword.delegate = self
        EtCurrentPassword.delegate = self
        EtConfirmPassword.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        EtNewPassword.resignFirstResponder()
        EtCurrentPassword.resignFirstResponder()
        EtConfirmPassword.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
